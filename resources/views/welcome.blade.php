<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- <link rel="icon" href="../../../../favicon.ico"> -->

    <title>{{ config('app.name')}}</title>

    <!-- Bootstrap core CSS -->
    <!-- <link href="../../dist/css/bootstrap.min.css" rel="stylesheet"> -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- Custom styles for this template -->
    <link href="{{ asset('css/cover.css') }}" rel="stylesheet">
  </head>

  <body class="text-center">
    
    <div class="cover-container d-flex w-100 h-100 p-3 mx-auto flex-column">
    @if (Route::has('login'))
      <header class="masthead mb-auto">
      <div class="inner">
      @if (Auth::check())
        <a href="{{ url('/home') }}">Home</a>
      @else
          <h3 class="masthead-brand">{{ config('app.name') }}</h3>
          <nav class="nav nav-masthead justify-content-center">
            <a class="nav-link" href="{{ url('/login') }}">Login</a>
            <a class="nav-link" href="{{ url('/register') }}">Register</a>
          </nav>        
        </div>
        @endif
      </header>
    @endif

@if(session()->has('notif'))
    <div class="row">
        <div class="alert alert-success">
            {{ session()->get('notif') }}
        </div>
    </div>
    @endif
      <main role="main" class="inner cover">
        <h1 class="cover-heading">E Voting</h1>
        <p class="lead">Do it the smart way!</p>
        <!-- <p class="lead">
          <a href="#" class="btn btn-lg btn-secondary">Login</a>
        </p> -->
      </main>

      <footer class="mastfoot mt-auto">
        <div class="inner">
          <p>Voting app, by <a href="http://biswashlamichhane.cf/">Biswash Lamichhane</a>.</p>
        </div>
      </footer>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="../../assets/js/vendor/popper.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
  </body>
</html>
