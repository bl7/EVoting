@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row center">
        <form class="form-signin" role="form" method="POST" action="{{ route('admin.login.submit') }}">
        <h1 class="h3 mb-3 font-weight-normal">ADMIN PANEL LOGIN!</h1>
            {{ csrf_field() }}
            <label for="email" class="sr-only">Email Address</label>
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email Address" required autofocus>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            <label for="password" class="sr-only">Password</label>
                <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            <div class="checkbox mb-3">
                <label>
                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                </label>
                <label>
                    <a class="btn btn-link" href="{{ route('admin.password.request') }}">
                    Forgot Your Password?
                    </a>
                </label>
            </div>
            <button type="submit" class="btn btn-primary">
                Login
            </button>
        </form>
    </div>
</div>
@endsection
