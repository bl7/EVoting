@extends('layouts.app')

@section('content')
@include('inc.navbar')
<div class="container">
    <div class="row center">
        <form class="form-signin" role="form" method="POST" action="{{ route('register') }}">
        <h1 class="h3 mb-3 font-weight-normal">Enter Credentials!</h1>
            {{ csrf_field() }}
                <label for="name" class="sr-only">Name</label>
                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Name" required autofocus>
                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                <label for="email" class="sr-only">Email Address</label>
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email Address"required>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                <label for="password" class="sr-only">Password</label>
                <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                <label for="password-confirm" class="sr-only">Confirm Password</label>
                <input id="password-confirm" type="password" class="form-control last" name="password_confirmation" placeholder="Confirm Password" required>
                <button type="submit" class="btn btn-lg btn-primary btn-block">
                    Register
                </button>
        </form>
    </div>
</div>
@endsection
