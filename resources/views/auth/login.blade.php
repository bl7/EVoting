@extends('layouts.app')
@section('content')
@include('inc.navbar')
<div class="container">
    <div class="row center">
    <form class="form-signin" role="form" method="POST" action="{{ route('login') }}">
    <h1 class="h3 mb-3 font-weight-normal">Enter Credentials!</h1>
        {{ csrf_field() }}        
            <label for="email" class="sr-only">E-Mail Address</label>
            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email address" required autofocus>
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            <label for="password" class="sr-only">Password</label>
            <input id="password" type="password" class="form-control last" name="password" placeholder="Password" required>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            <div class="checkbox mb-3">
                <label>
                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                </label>
                <label>
                <a class="btn btn-link" href="{{ route('password.request') }}">
                    Forgot Your Password?
                </a>
                </label>
            </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    </form>  
    </div>
</div>
@endsection
