@extends('layouts.app')

@section('content')
@include('inc.navbar')
@include('inc.sidebar-user')

<main role="main" id="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="container well">
    @if(session()->has('notif'))
    <div class="row">
        <div class="alert alert-success">
            {{ session()->get('notif') }}
        </div>
    </div>
    @endif
        @if(count($options)>1)
        @foreach ($options as $option)
        @if($loop->first)
            <h1 class="title"> {{ $option->poll->name}} </h1>
        @endif
                 
        <div class="row">
            <div class="col-lg-12">
                <div class="well">
                    <form role="form" method="POST" action="/testPoll" class="lead">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-lg-8 col-md-8">
                                <p class="lead">{{ $option->opt_name}}</p>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="hide">{{ $rec=VoteCount( $option->poll->id, $option->id ) }}</div>
                                <span> {{ $rec }} Votes</span>
                            </div>
                        </div>
                        <input type="hidden" name="userId" value="{{ Auth::user()->id}}">
                        <input type="hidden" name="electionId" value="{{ $option->poll->id}}">
                        <input type="hidden" name="candidateId" value="{{ $option->id}}">
                        <span class="hide">{{$resu=PollvotedOrNot(Auth::user()->id,$option->poll->id)}}</span> 
                        @if($resu==0)
                        <div class="d-flex justify-content-between align-items-center"> 
                            <div class="btn-group"> 
                                <button type="submit" class="btn btn-primary"> Vote</button>
                            </div>
                        </div>
                        @else
                        @endif 
                    </form>  
                </div>
            </div>            
        </div>
          
          
            @endforeach
            @else
            <div class="container">
                <h1 class="title"> Voting not available yet!!</h1>
            </div>
            @endif
    </div>
</main>
@endsection

