@extends('layouts.app')

@section('content')
@include('inc.navbar')
@include('inc.sidebar-user')

<main role="main" id="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
  <div class="container">
  @if(session()->has('notif'))
    <div class="row">
        <div class="alert alert-success">
            {{ session()->get('notif') }}
        </div>
    </div>
    @endif
    <span class="hide">{{ $posts=allArticles() }}</span>
    @if(count($posts)>0)
    <h2 class="title center">News</h2>
    @foreach($posts as $post)
    <div class="container well">
      <div class="center">
      <img style="width:100%" src="/storage/images/{{ $post->image }}" alt="">
      </div>
      <h2>
        {{ $post->title}}
      </h2>
      <p> 
        {!! $post->body !!}
      </p> 
      
      <small>Uploaded on: {{ $post->created_at}}</small>       
    </div>
    @endforeach
    {{ $posts->links() }}
    @else
    <p>No recent articles to show!!</p>
    @endif
  </div>
</main>

@endsection