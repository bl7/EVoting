@extends('layouts.app')

@section('content')
@include('inc.navadmin')
@include('inc.sidebar')

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="container">
    @if(session()->has('notif'))
    <div class="row">
        <div class="alert alert-success">
            {{ session()->get('notif') }}
        </div>
    </div>
    @endif
        @if(count($articles)>0)
        @foreach($articles as $article)
        <div class="well">
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <img style="width:100%" src="/storage/images/{{ $article->image }}" alt="">
                </div>
                <div class="col-md-8 col-sm-8">
                    <h3>{{ $article->title }}</h3>  
                    <div class="btn-group" role="group" aria-label="EditDelete">
                        <a class= "btn btn-info" href="/articleedit/{{$article->id}}">Edit</a>
                        <a class= "btn btn-danger" href="/adminarticledelete/{{$article->id}}">Delete</a>
                    </div>                   
                </div>
            </div>  
        </div> 
        @endforeach
        @else
        <p>No articles!</p>
        @endif
    </div>
</main>
@endsection
