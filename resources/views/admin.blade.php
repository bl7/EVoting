@extends('layouts.app')

@section('content')
@include('inc.navadmin')
@include('inc.sidebar')

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="container">
    @if(session()->has('notif'))
    <div class="row">
        <div class="alert alert-success">
            {{ session()->get('notif') }}
        </div>
    </div>
    @endif
    <h2 class="title">Register Election</h2>
    <form class="well" role="form" method="POST" action="/election/create">
        {{ csrf_field() }}
        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name">Election</label>
            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('desc') ? ' has-error' : '' }}">
            <label for="desc">Description</label>
            <textarea id="desc" rows="3" class="form-control" name="desc" value="{{ old('desc') }}" ></textarea>
            @if ($errors->has('desc'))
                <span class="help-block">
                    <strong>{{ $errors->first('desc') }}</strong>
                </span>
            @endif  
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">
                Add Election
            </button>
        </div>
    </form>    
    </div>

     <div class="container">
    <h2 class="title">Register Poll</h2>
    <form class="well" role="form" method="POST" action="/poll/create">
        {{ csrf_field() }}
        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            <label for="name">Poll</label>
            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">
                Add Poll
            </button>
        </div>
    </form>    
    </div>

     <div class="container">
    <h2 class="title">Add Article</h2>
    <form class="well" enctype="multipart/form-data" role="form" method="POST" action="/article/create">
        {{ csrf_field() }}
        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
            <label for="title">Title</label>
            <input id="title" type="text" class="form-control" name="title" value="{{ old('title') }}" >
            @if ($errors->has('title'))
                <span class="help-block">
                    <strong>{{ $errors->first('title') }}</strong>
                </span>
            @endif
        </div>
       

        <div class="form-group">
            {{Form::label('body', 'Body')}}
            {{Form::textarea('body', '', ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Body Text'])}}
        </div>

    <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }} ">
        <label for="image">{{ __('Image (optional)') }}</label>
        <input type="file" class="form-control" name="image" id="image">
        @if ($errors->has('image'))
            <span class="help-block">
                <strong>{{ $errors->first('image') }}</strong>
            </span>
        @endif  
    </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary">
                Upload Article
            </button>
        </div>
    </form>    
    </div>

</main>
@endsection
