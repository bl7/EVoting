@extends('layouts.app')

@section('content')
@include('inc.navadmin')
@include('inc.sidebar')

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="container">
    @if(session()->has('notif'))
    <div class="row">
        <div class="alert alert-success">
            {{ session()->get('notif') }}
        </div>
    </div>
    @endif
    <h2 class="title">Edit Article</h2>
    {!! Form::open(['action' => ['ArticlesController@update', $article->id], 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class' =>'well']) !!}
        <div class="form-group">
            {{Form::label('title', 'Title')}}
            @if($errors->has('title'))
                <div class="help-block">
                    {{ $errors->first('title')}}
                </div>
            @endif
            {{Form::text('title', $article->title, ['class' => 'form-control', 'placeholder' => 'Title'])}}
            
        </div>
        <div class="form-group">
            {{Form::label('body', 'Body')}}
            @if($errors->has('body'))
                <div class="help-block">
                    {{ $errors->first('body')}}
                </div>
            @endif
            {{Form::textarea('body', $article->body, ['id' => 'article-ckeditor', 'class' => 'form-control', 'placeholder' => 'Body Text'])}}
        </div>
        <div class="form-group">
            {{Form::file('image',['class'=>'form-control'])}}
        </div>
        {{Form::hidden('_method','PUT')}}
        {{Form::submit('Submit', ['class'=>'btn btn-primary'])}}
    {!! Form::close() !!}
    </div>
</main>
@endsection
