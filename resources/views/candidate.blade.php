@extends('layouts.app')

@section('content')
@include('inc.navadmin')
@include('inc.sidebar')

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <section>
    <div class="container">
    @if(session()->has('notif'))
    <div class="row">
        <div class="alert alert-success">
            {{ session()->get('notif') }}
        </div>
    </div>
    @endif
    <h2 class="title">Add Candidates</h2>
        <form role="form"  enctype="multipart/form-data" class="well" method="POST" action="/candidate/create">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="election">Candidates for</label>
                <select name="election" id="election" class="form-control">
                        @if(count($elections)>0)
                            @foreach($elections as $election)
                                <option value="{{ $election->id }}">
                                {{ $election->name }}
                                </option>
                            @endforeach
                        @endif
                </select>
            </div>
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="candidate">Candidate</label>
                <input id="candidate" type="text" class="form-control" name="candidate" value="{{ old('candidate') }}" required autofocus>
                    @if ($errors->has('candidate'))
                        <span class="help-block">
                            <strong>{{ $errors->first('candidate') }}</strong>
                        </span>
                    @endif
                </div>
            <div class="form-group{{ $errors->has('can_image') ? ' has-error' : '' }} ">
                <label for="can_image">{{ __('Image (optional)') }}</label>
                <input type="file" class="form-control" name="can_image" id="can_image">
                @if ($errors->has('can_image'))
                <span class="help-block">
                <strong>{{ $errors->first('can_image') }}</strong>
                </span>
                @endif  
            </div>

            <div class="form-group">
                    <button type="submit" class="btn btn-primary">
                        Add Candidate
                    </button>
            </div>
        </form>  
    </div>
    
        <div class="container">
        <h2 class="title">Existing Elections</h2>
        @if(count($elections)>0)
            <div class="row">
            @foreach($elections as $election)
                <div class="col-sm-4">
                    <div class="well">
                        <h3>{{ $election->name }}</h3>
                        <small>{{ $election-> desc}}</small> <br>
                        <div class="hide"> {{ $status= completedOrNot($election->id)}}</div>
                        <div class="btn-group" role="group" aria-label="EditDelete">
                        @if($status==0)
                        @else
                            <a class= "btn btn-info" href="/statusupdate/{{$election->id}}">Stop & Publish</a>
                        @endif
                            <a class= "btn btn-danger" href="/electiondelete/{{$election->id}}">Delete</a>
                        </div>    
                    </div>
                </div>
            @endforeach
            </div>
            @else
            <p>No elections registered yet!!</p>
        @endif
        </div>
    </section>        
</main>
@endsection



