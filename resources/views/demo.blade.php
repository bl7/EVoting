@extends('layouts.app')

@section('content')
@include('inc.navbar')
@include('inc.sidebar-user')

<main role="main" id="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
  <div class="container">
  @if(session()->has('notif'))
    <div class="row">
        <div class="alert alert-success">
            {{ session()->get('notif') }}
        </div>
    </div>
    @endif
  <h3 class="title center">Elections</h3>
    <div class="row">
    <span class="hide">{{ $elections= allElections() }}</span>
      @if(count($elections)>0)
      @foreach($elections as $election)
        <div class="col-lg-4 col-md-6 col-sm-6">
          <div class="card">
            <div class="card-body">
              <h3 class="card-title"> {{ $election->name }}</h3>    
                <div class="d-flex justify-content-between align-items-center"> 
                  <div class="btn-group"> 
                  <a href="elections/{{ $election->id }}" class="btn btn-sm btn-primary"> View
                  </a>
                </div> 
            </div>
          </div>
          </div>
      </div>
      @endforeach
      @else 
      <p>No election registered yet!!</p>
      @endif
      </div>
    </div>


  </main>
@endsection

