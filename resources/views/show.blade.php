@extends('layouts.app')

@section('content')
@include('inc.navbar')
@include('inc.sidebar-user')

<main role="main" id="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="container well">   
    @if(session()->has('notif'))
    <div class="row">
        <div class="alert alert-success">
            {{ session()->get('notif') }}
        </div>
    </div>
    @endif
    @if(count($candidates)>1)
    @foreach ($candidates as $candidate)
    <div class="hide"> {{ $status= completedOrNot($candidate->election->id)}}</div>
    @if($status==0)
    <span class="hide">{{$resu=votedOrNot(Auth::user()->id,$candidate->election->id)}}</span> 
        @if($loop->first)
            <h1 class="title"> {{ $candidate->election->name}} </h1>
            <p class="lead">  {!! $candidate->election->desc !!}</p>
            @if($resu==0)
        @else
        <div class="hide"> {{ $total= TotalElectionVotes($candidate->election->id)}}</div>
        <small> {{ $total }} people voted</small>
        @endif
            <hr>   
        @endif  
        <form role="form" method="POST" action="/test" class="lead">
        {{ csrf_field() }}
        <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="card">
            <img style="width:100%" src="/storage/can_images/{{ $candidate->can_image }}" alt="">
                <div class="card-body">
                    <p class="card-title">{{ $candidate->can_name}}</p>
                            @if($resu==0)
                        <input type="hidden" name="userId" value="{{ Auth::user()->id}}">
                        <input type="hidden" name="electionId" value="{{ $candidate->election->id}}">
                        <input type="hidden" name="candidateId" value="{{ $candidate->id}}">
                        <div class="d-flex justify-content-between align-items-center"> 
                            <!-- <div class="btn-group"> 
                                <button type="submit" class="btn btn-primary"> Vote</button>
                            </div> -->
                        </div> 
                        @else
                        <div class="hide"> {{ $amt=ElectionVoteCount($candidate->election->id, $candidate->id)}}</div> 
                        <h2 class="lead"> {{ $amt }} Votes</h2>
                        @endif               
                    </div>            
                </div>
            </div>  
        </form> 
    @else
        <span class="hide">{{$resu=votedOrNot(Auth::user()->id,$candidate->election->id)}}</span> 
        @if($loop->first)
            <h1 class="title"> {{ $candidate->election->name}} </h1>
            <p class="lead">  {!! $candidate->election->desc !!}</p>
            @if($resu==0)
        @else
        <h5>**You have already voted!</h5>
        @endif
            <hr>   
        @endif  
        <form role="form" method="POST" action="/test" class="lead">
        {{ csrf_field() }}
        <div class="col-lg-4 col-md-6 col-sm-6">
            <div class="card">
            <img style="width:100%" src="/storage/can_images/{{ $candidate->can_image }}" alt="">
                <div class="card-body">
                    <p class="card-title">{{ $candidate->can_name}}</p>
                            @if($resu==0)
                        <input type="hidden" name="userId" value="{{ Auth::user()->id}}">
                        <input type="hidden" name="electionId" value="{{ $candidate->election->id}}">
                        <input type="hidden" name="candidateId" value="{{ $candidate->id}}">
                        <div class="d-flex justify-content-between align-items-center"> 
                            <div class="btn-group"> 
                                <button type="submit" class="btn btn-primary"> Vote</button>
                            </div>
                        </div> 
                        @else
                        @endif               
                    </div>            
                </div>
            </div>  
        </form> 
        @endif
        @endforeach
        @else
        <div class="container">
            <h1 class="title"> Candidates not pronounced yet</h1>
        </div>
        @endif
    </div>
</main>
@endsection

