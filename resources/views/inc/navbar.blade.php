
<header>
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <div class="container">                
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <svg width="30" height="30">
                    <path d="M0,4 25,4" stroke="#DAE0E2" stroke-width="3"/>
                    <path d="M0,10 25,10" stroke="#DAE0E2" stroke-width="3"/>
                    <path d="M0,16 25,16" stroke="#DAE0E2" stroke-width="3"/>
                    </svg>
                </button>
                <!-- Branding Image -->
                @if (Auth::guest())
                <div class="navbar-brand">
                {{ config('app.name', 'EVoting') }}
                </div>
                @else
                <!-- <a href="#" onclick="openSlideMenu()"> -->
                <div class="navbar-brand mb-r"> 
                <a href="#" onclick="openSlideMenu()">
                    <svg width="30" height="30">
                    <path d="M0,4 20,4" stroke="#DAE0E2" stroke-width="4"/>
                    <path d="M0,10 20,10" stroke="#DAE0E2" stroke-width="4"/>
                    <path d="M0,16 20,16" stroke="#DAE0E2" stroke-width="4"/>
                    </svg>
                </a>
                </div>
                <div class="navbar-brand">
                {{ config('app.name', 'EVoting') }}
                </div> 
                @endif  
            </div>
            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    &nbsp;
                </ul>
                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ route('login') }}">Login</a></li>
                        <li><a href="{{ route('register') }}">Register</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                <li>
                                    <a href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                        
                    @endif
                </ul>
            </div>
        </div>
    </nav>
</header>