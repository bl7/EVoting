<nav class="col-md-2 d-none d-md-block  sidebar">
        <div class="sidebar-sticky">
          <ul class="nav flex-column">
            <li class="nav-item">
              <a class="nav-link active" href="/admin">
                Home <span class="sr-only">(current)</span>
              </a>
            </li>

            <li class="nav-item">
              <a class="nav-link" href="/admin/candidate">
                Election
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/admin/poll">
                Polls
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/admin/articles">
                Articles
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/results">
                Publish results
              </a>
            </li>
          </ul>

        </div>
      </nav>