@extends('layouts.app')

@section('content')
@include('inc.navadmin')
@include('inc.sidebar')

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <section>
    <div class="container">
    @if(session()->has('notif'))
    <div class="row">
        <div class="alert alert-success">
            {{ session()->get('notif') }}
        </div>
    </div>
    @endif
    <h2 class="title">Add Options</h2>
        <form role="form" class="well" method="POST" action="/option/create">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="poll">Polls for</label>
                <select name="poll" id="poll" class="form-control">
                        @if(count($polls)>0)
                            @foreach($polls as $poll)
                                <option value="{{ $poll->id }}">
                                {{ $poll->name }}
                                </option>
                            @endforeach
                        @endif
                </select>
            </div>
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="option">Option</label>
                <input id="option" type="text" class="form-control" name="option" value="{{ old('option') }}" required autofocus>
                    @if ($errors->has('option'))
                        <span class="help-block">
                            <strong>{{ $errors->first('option') }}</strong>
                        </span>
                    @endif
                </div>
            <div class="form-group">
                    <button type="submit" class="btn btn-primary">
                        Add Option
                    </button>
            </div>
        </form>  
    </div>

        <div class="container">
        <h2 class="title">Existing Polls</h2>
        @if(count($polls)>0)
            <div class="row">
            @foreach($polls as $poll)
                <div class="col-lg-12">
                    <div class="well">
                        <h3>{{ $poll->name }}</h3> 
                        <div class="btn-group" role="group" aria-label="EditDelete">
                            <a class= "btn btn-danger" href="/polldelete/{{$poll->id}}">Delete</a>
                        </div>    
                    </div>
                </div>
            @endforeach
            </div>
            @else
            <p>No polls registered yet!!</p>
        @endif
        </div>
    </section>         
    </main>
@endsection



