<?php

use App\Vote;
use App\PollVote;
use App\Article;
use App\Election;
function votedOrNot($userId, $electionId){
    $res = Vote::where([['user_id','=',$userId],['election_id', '=', $electionId]]);
     $res->get();
    if(($res->count()==0)){
        $chk=0;
    }else{
        $chk=1;
    }
    return $chk;
}

function completedOrNot($electionId){
    $rest= Election::where([['id','=',$electionId],['completed','=',0]]);
    $rest->get();
    if(($rest->count()==0)){
        $chk=0;
    }else{
        $chk=1;
    }
    return $chk;
}

function PollVotedOrNot($userId, $pollId){
    $resp = PollVote::where([['user_id','=',$userId],['poll_id', '=', $pollId]]);
     $resp->get();
    if(($resp->count()==0)){
        $chkp=0;
    }else{
        $chkp=1;
    }
    return $chkp;
}

function VoteCount($pollId, $optionId){
    $cnt=PollVote::where([['poll_id','=',$pollId],['option_id', '=', $optionId]]);
    $cnt->get();
    $amt=($cnt->count());
    return $amt;
}

function allArticles(){
    $articles= Article::orderBy('created_at','desc')->paginate(3);
    return $articles;
}

function ElectionVoteCount($electionId, $candidateId){
    $cnt=Vote::where([['election_id','=',$electionId],['candidate_id', '=', $candidateId]]);
    $cnt->get();
    $amt=($cnt->count());
    return $amt;
}

function TotalElectionVotes($electionId){
    $cnt=Vote::where([['election_id','=',$electionId]]);
    $cnt->get();
    $amt=($cnt->count());
    return $amt;
}


