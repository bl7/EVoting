<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Poll;


class Option extends Model
{
    public function poll(){
        return $this->belongsTo(Poll::class);
    }
}
