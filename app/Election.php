<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Election extends Model
{

    protected $fillable = ['name', 'desc', 'completed'];

    public function candidates(){
        return $this->hasMany(Candidate::class);
    }

}
