<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Election;

class Candidate extends Model
{
    public function election(){
        return $this->belongsTo(Election::class);
    }

    
}