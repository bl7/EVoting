<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Candidate;

class CandidatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $candidates= Candidate::all();
        return view('elections')->with('candidates', $candidates);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'candidate'=>'required',
            'can_image'=>'image|nullable|max:1999'
        ]); 
        if($request->hasFile('can_image')){
            // name with extension
            $filenameWithExt=$request->file('can_image')->getClientOriginalName();
            // only name
            $filename=pathinfo($filenameWithExt,PATHINFO_FILENAME);
            // get extension
            $extension=$request->file('can_image')->getClientOriginalExtension();
            // filename to store
            $fileToStore=$filename.'_'.time().'.'.$extension;
            // upload image
            $path=$request->file('can_image')->storeAs('public/can_images',$fileToStore);
        }else{
            $fileToStore='noimage.jpg';
        }
        $candidate=new Candidate;
        $candidate->can_name=$request->input('candidate');
        $candidate->election_id=$request->input('election'); 
        $candidate->can_image=$fileToStore;  
        $candidate->save();
        session()->flash('notif','Candidate added to the election!');
        // return $request->input('name');
        return redirect('/admin/candidate');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $candidate=Candidate::find($id);
        $candidate->delete();
        return redirect('/admin/electionedit');
    }
}
