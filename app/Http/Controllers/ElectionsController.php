<?php

namespace App\Http\Controllers;
use App\Candidate;
use App\Election;
use Illuminate\Http\Request;
// use App\Http\Controllers\Auth;
class ElectionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    public function index()
    {
        // $this->middleware('auth:admin');
        $elections= Election::all();
        // return $elections;
        return view('candidate')->with('elections', $elections);
        
    }
    public function forUser(){
        $elections= Election::all();
        // return $elections;
        return view('elections')->with('elections', $elections);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $completed=0;

        $election=new Election;
        $election->name=$request->input('name'); 
        $election->desc=$request->input('desc');
        $election->completed=$completed; 
        $election->save();
        session()->flash('notif','A new election created!');
        // return $request->input('name');
        return redirect('/admin');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Election  $election
     * @return \Illuminate\Http\Response
     */
    public function show(Election $election)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Election  $election
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $election= Election::find($id);
        return view('electionedit')->with('election',$election);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Election  $election
     * @return \Illuminate\Http\Response
     */
    public function stupdate(Request $request)
    {
        $id=$request->id;

        $election = Election::find($id);
        $election->completed = 1;
        $election->save();
        session()->flash('notif','Election has been updated and result has been published for users!');
        return redirect('/admin/candidate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Election  $election
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $election=Election::find($id);
        
        $election->delete();
        session()->flash('notif','Election has been deleted!');
        return redirect('/admin/candidate');
    }

    
}


