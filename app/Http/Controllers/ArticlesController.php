<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
class ArticlesController extends Controller
{
     
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles= Article::all();
        return view('articles')->with('articles', $articles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title'=>'required',
            'body'=>'required',
            'image'=>'image|nullable|max:1999'
        ]); 

        if($request->hasFile('image')){
            // name with extension
            $filenameWithExt=$request->file('image')->getClientOriginalName();
            // only name
            $filename=pathinfo($filenameWithExt,PATHINFO_FILENAME);
            // get extension
            $extension=$request->file('image')->getClientOriginalExtension();
            // filename to store
            $fileToStore=$filename.'_'.time().'.'.$extension;
            // upload image
            $path=$request->file('image')->storeAs('public/images',$fileToStore);
        }else{
            $fileToStore='noimage.jpg';
        }

        $article= new Article;
        $article->title=$request->input('title');
        $article->body=$request->input('body');
        $article->image=$fileToStore;  
        $article->save();
        session()->flash('notif','A new article created!');
        return redirect('/admin');      
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show($article_id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit($article_id)
    {   
        $article= Article::find($article_id);
        return view('articleedit')->with('article',$article);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'body' => 'required'
        ]);

         // Handle File Upload
        if($request->hasFile('image')){
            // Get filename with the extension
            $filenameWithExt = $request->file('image')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('image')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore= $filename.'_'.time().'.'.$extension;
            // Upload Image
            $path = $request->file('image')->storeAs('public/images', $fileNameToStore);
        }

        // Create Post
        $article = Article::find($id);
        $article->title = $request->input('title');
        $article->body = $request->input('body');
        if($request->hasFile('image')){
            $article->image = $fileNameToStore;
        }
        $article->save();
        session()->flash('notif','Article has been updated!');
        return redirect('/admin')->with('success', 'Post Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article = Article::find($id);

        // Check for correct user
       

        if($article->image != 'noimage.jpg'){
            // Delete Image
            Storage::delete('public/images/'.$article->image);
        }
        
        $article->delete();
        session()->flash('notif','Article has been deleted!');
        return redirect('/admin');
    }
}

