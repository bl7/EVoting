<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Election;
use App\Candidate;
// use App\Http\Auth\CandidatesController;
class CandidateElectionController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(){
        $elections=Election::all();
        return view('elections')->with('elections', $elections);
    }

    public function show($election_id){
        $candidates=Election::find($election_id)->candidates;
        return view('show',compact('candidates'));   
    }
    
    public function storeTest(Request $request){
        $vote=$request->all();
        return $vote;
    }
}
