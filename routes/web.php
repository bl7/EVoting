<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});


Route::get('/demo', function () {
  return view('demo');
});


Auth::routes();


Route::get('/home', 'HomeController@index');
Route::get('/users/logout', 'Auth\LoginController@userLogout')->name('user.logout');
// Route::get('/candidate',function(){
//   return view('candidate');
// });
Route::prefix('admin')->group(function() {
  Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
  Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
  Route::get('/', 'AdminController@index')->name('admin.dashboard');
  Route::get('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');

  // Password reset routes
  Route::post('/password/email', 'Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
  Route::get('/password/reset', 'Auth\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
  Route::post('/password/reset', 'Auth\AdminResetPasswordController@reset');
  Route::get('/password/reset/{token}', 'Auth\AdminResetPasswordController@showResetForm')->name('admin.password.reset');

  Route::get('/candidate','ElectionsController@index');
  Route::get('/poll','PollsController@index');
  Route::get('/articles','ArticlesController@index');
});

Route::post('/election/create','ElectionsController@store');
Route::post('/poll/create','PollsController@store');
Route::post('/election/show/{$election_id}','ElectionsController@show');


// form routes
Route::post('/candidate/create','CandidatesController@store');
Route::post('/option/create','OptionsController@store');
Route::post('/article/create','ArticlesController@store');


// cast votes
Route::post('/test','VoteController@store');
Route::post('/testPoll','PollVoteController@store');


Route::get('elections','CandidateElectionController@index');
Route::get('polling','OptionPollController@index');

Route::get('elections/{election_id}','CandidateElectionController@show');
Route::get('polling/{poll_id}','OptionPollController@show');

// edit routes
Route::get('/articleedit/{id}','ArticlesController@edit');
Route::get('/electionedit/{id}','ElectionsController@edit');
Route::get('/polledit/{id}','PollsController@edit');

// updateroutes
Route::post('/electionupdate/{id}','ElectionsController@update');
Route::get('/statusupdate/{id}','ElectionsController@stupdate');
Route::post('/pollupdate/{id}','PollsController@update');

// resource route
Route::resource('articles', 'ArticlesController');
// Route::resource('polls', 'PollsController');
// Route::resource('elections', 'ElectionsController');


// delete routes
Route::get('/adminarticledelete/{id}','ArticlesController@destroy');
Route::get('/electiondelete/{id}','ElectionsController@destroy');
Route::get('/polldelete/{id}','PollsController@destroy');


