<?php
namespace Tests\Unit;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Article;

class deleteArticleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_delete_article()
    {
      $find_article = Article::where('id', '1')->first();
      $delete = Article::destroy($find_article['id']);

      $this->addToAssertionCount(1);
    }
}

