<?php
namespace Tests\Unit;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;

class deleteUserTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_delete_user()
    {
      $find_user = User::where('email', 'test@test.com')->first();
      $delete = User::destroy($find_user['id']);

      $this->addToAssertionCount(1);
    }
}
